<?php
$btc_price = file_get_contents('https://api.coinmarketcap.com/v2/ticker/1/?convert=USD');
$xcp_price = file_get_contents('https://api.coinmarketcap.com/v2/ticker/132/?convert=USD');

$btc_price = json_decode($btc_price, true);
$xcp_price = json_decode($xcp_price, true);

$symbol_btc = $btc_price["data"]["symbol"];
$btc_usd = $btc_price["data"]["quotes"]["USD"]["price"];
$btc_1h = $btc_price["data"]["quotes"]["USD"]["percent_change_1h"];
$btc_24h = $btc_price["data"]["quotes"]["USD"]["percent_change_24h"];
$btc_7d = $btc_price["data"]["quotes"]["USD"]["percent_change_7d"];

$json_prices[0]["symbol"] = $symbol_btc;
$json_prices[0]["price_usd"] = $btc_usd;
$json_prices[0]["1h"] = $btc_1h;
$json_prices[0]["24h"] = $btc_24h;
$json_prices[0]["7d"] = $btc_7d;

$symbol_xcp = $xcp_price["data"]["symbol"];
$xcp_usd = $xcp_price["data"]["quotes"]["USD"]["price"];
$xcp_1h = $xcp_price["data"]["quotes"]["USD"]["percent_change_1h"];
$xcp_24h = $xcp_price["data"]["quotes"]["USD"]["percent_change_24h"];
$xcp_7d = $xcp_price["data"]["quotes"]["USD"]["percent_change_7d"];

$json_prices[1]["symbol"] = $symbol_xcp;
$json_prices[1]["price_usd"] = $xcp_usd;
$json_prices[1]["1h"] = $xcp_1h;
$json_prices[1]["24h"] = $xcp_24h;
$json_prices[1]["7d"] = $xcp_7d;

echo json_encode($json_prices);
?>
