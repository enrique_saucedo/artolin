<?php

$tokens = file_get_contents('https://www.artolin.org/json/artlist.json');
$tokens = json_decode($tokens, true);

$assets_name = array_keys($tokens);
$images_url = array_values($tokens);

$artolin_tokens = array();

for ($i=0; $i < count($assets_name) ; $i++) {
  $artolin_tokens[$i]["asset"] = $assets_name[$i];
  $artolin_tokens[$i]["url"] = $images_url[$i];
}

echo json_encode($artolin_tokens, JSON_UNESCAPED_SLASHES);
?>
