<?php

require 'includes/Client.php';
use JsonRPC\Client;

include("settings.php");

$client = new Client($cp_server);
$client->authentication($cp_user, $cp_password);

$address = ((isset($_POST['address'])) ? ($_POST['address']) : (''));

$filters = array(array('field' => 'address', 'op' => '==', 'value' => $address));
$balances_result = $client->execute('get_balances', array('filters' => $filters));

$balances_parsed = array();

//check if divisible
$assets = array();
for($i=0; $i < count($balances_result); $i++){
  $assets[$i] = $balances_result[$i]["asset"];
}

$filters = array(array('field' => 'asset', 'op' => 'IN', 'value' => $assets));
$issuances_result = $client->execute('get_issuances', array('filters' => $filters, 'filterop' => "AND"));
$assets_divisible = array();

for($i=0; $i < count($issuances_result); $i++){
  $assets_divisible[$issuances_result[$i]["asset"]] = $issuances_result[$i]["divisible"];
}

for($i=0; $i < count($balances_result); $i++){
  $balances_parsed[$i]["asset"] = $balances_result[$i]["asset"];

  if($assets_divisible[$balances_parsed[$i]["asset"]] == 1) {
    $balances_result[$i]["quantity"] /= 100000000;
    $balances_parsed[$i]["amount"] = number_format($balances_result[$i]["quantity"], 2, ".", "");
  } else {
    if ($balances_parsed[$i]["asset"]=="XCP") {
      $balances_parsed[$i]["amount"] = number_format(($balances_result[$i]["quantity"]/100000000), 2, ".", "");
    } else {
      $balances_parsed[$i]["amount"] = number_format($balances_result[$i]["quantity"], 0, ".", "");
    }
  }
}

$amount_btc = file_get_contents('https://testnet.blockexplorer.com/api/addr/'.$address.'/balance');

$balance_btc = array();
$balance_btc["asset"] = "BTC";
$balance_btc["amount"] = ($amount_btc/100000000);

$balance_btc = array();

array_unshift($balances_parsed , $balance_btc);

echo json_encode($balances_parsed);
?>
