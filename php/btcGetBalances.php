
<html lang="en">
<head>
</head>


<?php

require 'includes/Client.php';
use JsonRPC\Client;

include("settings.php");

    $client = new Client($cp_server);
    $client->authentication($cp_user, $cp_password);

    $filters = array(array('field' => 'address', 'op' => 'IN', 'value' => $address));
    $balances_result = $client->execute('get_balances', array('filters' => $filters));

    $balances_parsed = array();

    //check if divisible
    $assets = array();
    for($i=0; $i < count($balances_result); $i++){
        $assets[$i] = $balances_result[$i]["asset"];
    }

    $filters = array(array('field' => 'asset', 'op' => 'IN', 'value' => $assets));
    $issuances_result = $client->execute('get_issuances', array('filters' => $filters, 'filterop' => "AND"));
    $assets_divisible = array();

    for($i=0; $i < count($issuances_result); $i++){
        $assets_divisible[$issuances_result[$i]["asset"]] = $issuances_result[$i]["divisible"];
    }

    for($i=0; $i < count($balances_result); $i++){
        $balances_parsed[$i]["asset"] = $balances_result[$i]["asset"];
        if($assets_divisible[$balances_parsed[$i]["asset"]] == 1) {
            $balances_result[$i]["quantity"] /= 100000000;
            $balances_parsed[$i]["amount"] = number_format($balances_result[$i]["quantity"], 8, ".", "");
        } else {
            $balances_parsed[$i]["amount"] = number_format($balances_result[$i]["quantity"], 0, ".", "");
        }
    }

    echo "s";
//    $result = $client->execute('create_cancel', array('offer_hash' => 'f3f817a070225bc31139fb462c9a7d613dface5dfdef62a1fbda213b9f0de8bf', 'source' => 'mgGMiRrhTxyZufJeX6GBGoQMjkbLg1GZpa'));

 ?>
</body>
</html>
<?php
/*

$result2 = $client->execute('create_issuance', array('source' => 'mvv6WMYEzv1QXoqru8RDQt3ybb6xbD4ZFs', 'asset' => 'POLLO', 'quantity' => 10, 'divisible' => false, 'description' => 'Para todos los pollos especiales', 'transfer_destination' => null));

echo '<script type="text/javascript">',
     'document.getElementById("sendfeedback").innerHTML += "<br />";',
     'document.getElementById("sendfeedback").innerHTML += "'.$result2.'";',
     '</script>';*/

?>
