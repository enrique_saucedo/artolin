//API functions rely on 3rd party services
function getBalances() {
  //asset balances and xcp from xchain
  $.getJSON("https://testnet.xchain.io/api/balances/" + address, function(data) {
    xcp_balance = "0"; //error if address not used (assume any error means 0 XCP)
    balances = [];
    $.each(data.data, function(key, val) {
      var asset = val.asset;
      var asset_long = val.asset_longname;
      var quant = val.quantity;
      var estimated = val.estimated_value.usd;
      if (asset == "XCP") {
        xcp_balance = quant;
      } else if (asset_long == "") {
        balances.push([asset, quant, asset, estimated]); //use latter index for sends (subassets have a numeric equivialent)
      } else {
        balances.push([asset_long, quant, asset, estimated]);
      }
    });
    displayBalances();
  });

  //btc balance from blockexplorer
  $.getJSON("https://testnet.blockexplorer.com/api/addr/" + address + "/balance", function(data) {
    btc_balance = data / 100000000;
    displayBalances();
  });

}

function getOlin(){

  if (btc_balance > 0.0008) {

    var btc_qty = btc_balance - 0.0006;

    btc_qty = Math.floor(btc_qty * 100) / 100;

    burn(address, btc_qty);

  }
}


function getPrices() {
  //coin prices from coinmarketcap
  //asset name may be either id or symbol
  $.getJSON("https://api.coinmarketcap.com/v1/ticker/?start=0&limit=310", function(data) {
    prices = [];
    $.each(data, function(key, val) {
      var id = val.id.toUpperCase();
      var symbol = val.symbol.toUpperCase();
      var price_usd = Number(val.price_usd);
      var price_btc = Number(val.price_btc);
      if (symbol == "BTC") {
        btc_usd = price_usd;
      } else if (symbol == "XCP") {
        xcp_usd = price_usd;
        xcp_btc = price_btc;
      } else {
        prices.push([id, price_usd, price_btc]);
        prices.push([symbol, price_usd, price_btc]);
      }
    });
    displayBalances();
    //displayPriceInfo();
  });
}

// *** Below functions display main wallet view
function displayWallet() {
//  var output = "";

  var rows = document.getElementsByClassName("row");
  rows[0].innerHTML = "";
  rows[1].innerHTML = "";
  rows[2].innerHTML = "";

  document.getElementById('dashboard').classList.add("active");
  document.getElementById('gallery').classList.remove("active");
  document.getElementById('history').classList.remove("active");
  document.getElementById('order').classList.remove("active");
  document.getElementById('myorder').classList.remove("active");
  document.getElementById('transaction').classList.remove("active");
  document.getElementById('wallet').classList.remove("active");

  var walletAddress = document.getElementsByClassName("navbar-brand");
  walletAddress[0].innerHTML = "Address: " + address;
  getBalances();
  getPrices();
  //displayPriceInfo();
  //displayWalletInfo();
}

function displayBalances() {
  var btc_val = btc_balance * btc_usd;
  btc_val = "$" + walletFormatUSD(btc_val);
  var xcp_val = xcp_balance * xcp_usd;
  xcp_val = "$" + walletFormatUSD(xcp_val);

  if (xcp_balance>1) {
    xcpVal = Math.floor(xcp_balance * 100) / 100;
    console.log(xcpVal);
    //post(address,"XCP",xcpVal,"OLINCOIN",parseInt(xcpVal/10));
  }

  var output = "";
  var output1 = "";

  output += '<div class="col-md-4 col-sm-6">';
/*  output += '<div class="card card-stats">';
  output += '<div class="card-header" style="width:82px; padding: 0; background-color:white">';
  output += '<img src="assets/img/logo-BTC.png">';
  output += '</div>';
  output += '<div class="card-content">';
  output += '<p class="category tbltok">BTC</p>';
  output += '<h4 class="title tblusd">'+ btc_val +'</h4>';
  output += '</div>';
  output += '<div class="card-footer">';
  output += '<div style="width: 100%;"><div class="col-md-6">';
  output += '<div class="stats">';
  output += '<i class="material-icons tblbal">account_balance_wallet</i> '+ bal_displ(btc_balance) +'</div></div>';
  output += '<div class="col-md-6"><button class="btn btn-primary btn-block" onclick="giveImage(\'BTC\');">SEND</button></div></div>';
  output += '</div>';
  output += '</div>';*/
  output += '</div>';

  /*output += '<div class="col-md-4 col-sm-6">';
  output += '<div class="card card-stats">';
  output += '<div class="card-header" style="width:82px; padding: 0; background-color:white">';
  output += '<img src="assets/img/logo-XCP.png">';
  output += '</div>';
  output += '<div class="card-content">';
  output += '<p class="category tbltok">XCP</p>';
  output += '<h4 class="title tblusd">'+ xcp_val +'</h4>';
  output += '</div>';
  output += '<div class="card-footer">';
  output += '<div style="width: 100%;"><div class="col-md-6">';
  output += '<div class="stats">';
  output += '<i class="material-icons tblbal">account_balance_wallet</i> '+ bal_displ(xcp_balance) +'</div></div>';
  output += '<div class="col-md-6"><button class="btn btn-primary btn-block" onclick="giveImage(\'XCP\');">SEND</button></div></div>';
  output += '</div>';
  output += '</div>';
  output += '</div>';*/
  output1 += '<div class="masonry">';

  output += '<div class="col-md-4 col-sm-6">';
  output += '<a class="btn btn-primary btn-block" onclick="getOlin();">FUND</a>';

  for (var i = 0; i < balances.length; i++) {
    var asset_val = "";
    for (var j = 0; j < prices.length; j++) {
      if (balances[i][0] == prices[j][0]) {
        asset_val = balances[i][1] * prices[j][1];
        asset_val = "$" + walletFormatUSD(asset_val);
      } else {
        asset_val = balances[i][3];
        asset_val = "$" + walletFormatUSD(asset_val);
      }
    }

    if (name_displ(balances[i][0]=='OLINCOIN')) {
      output += '<div class="card card-stats">';
      output += '<div class="card-header" style="width:82px; padding: 0; background-color:white">';
      output += '<img src="assets/img/logo-artolin.png">';
      output += '</div>';
      output += '<div class="card-content">';
      output += '<p class="category tbltok">OLINCOIN</p>';
      output += '<h4 class="title tblusd">'+ bal_displ(balances[i][1]) +'</h4>';
      output += '</div>';
      output += '<div class="card-footer">';
      output += '<div style="width: 100%;"><div class="col-md-6">';
      output += '<div class="stats">';
      output += '<i class="material-icons tblbal">account_balance_wallet</i> '+ asset_val +'</div></div>';
      output += '<div class="col-md-6"><button class="btn btn-primary btn-block" onclick="giveImage(\'OLINCOIN\');">SEND</button></div></div>';
      output += '</div>';
      output += '</div>';
      output += '</div>';
    }


    for (var x = 0; x < tokens.length; x++) {
      if ( tokens[x] == name_displ(balances[i][0]) ) {
        output1 += '<div class="item">';
        output1 += '<div class="card card-stats">';
        output1 += '<div class="card-content">';
        output1 += '<img src="'+url_tokens[x]+'">';
        output1 += '<div style="padding:15px;"><div class="col-sm-4">';
        output1 += '<h6 class="category tbltok">'+ name_displ(balances[i][0]) +'</h6></div>';
        output1 += '<div class="col-sm-8"><i class="material-icons tblbal">account_balance_wallet</i><span style="vertical-align: top;padding: 10px;">'+ balances[i][1]+'</span></div></div>';
        output1 += '</div>';
        output1 += '<div class="card-footer">';
        //output1 += '<div class="stats">';
        //output1 += '<i class="material-icons tblbal">account_balance_wallet</i> '+ bal_displ(balances[i][1]) +'</div>';
        output1 += '<div style="width: 100%;"><div class="col-md-6">';
        output1 += '<button class="btn btn-primary btn-block" onclick="imageOrder(\''+ name_displ(balances[i][0]) +'\');">SELL</button></div>';
        //output1 += '<button class="btn btn-primary btn-block" onclick="imageOrder(\''+ name_displ(balances[i][0]) +'\');">SELL</button></div>';
        output1 += '<div class="col-md-6"><button class="btn btn-primary btn-block" onclick="giveImage(\''+ name_displ(balances[i][0]) +'\');">SEND</button></div></div>';
        //output1 += '<a href="https://www.artolin.org/es/galeria/" class="btn btn-primary btn-block">VIEW ON GALLERY</a>';
        output1 += '</div>';
        output1 += '</div>';
        output1 += '</div>';
      }
    }
  }
   output1 += '</div>';

   output += '<div class="col-md-4 col-sm-6"></div>';


   var balancesRow = document.getElementsByClassName("row");
   balancesRow[1].innerHTML = output;
   balancesRow[2].innerHTML = output1;
}

function name_displ(asset) {
  if (asset.length > 22) {
    return asset.substring(0, 20) + "..";
  }
  return asset;
}

function bal_displ(number) {
  //Outputs a string with number formatted for wallet balance display
  //Always 8 digits after comma
  //Fractional part shall be smaller and grey (HTML5 format)
  //Thousands separator if >=10,000.00000000
  if (isNaN(number)) return "?";
  var formatted = parseFloat(number);
  formatted = formatted.toFixed(2);
  var split = formatted.toString().split('.');
  split[1] = "<span style=\"color:dimgray;font-size:100%;\">" + split[1].substring(0, 3) + "</span><span style=\"color:dimgray;font-size:90%;\">" + split[1].substring(3, 6) + "</span><span style=\"color:dimgray;font-size:75%;\">" + split[1].substring(
    6) + "</span>";
  if (split[0].length >= 5) {
    split[0] = split[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
  }
  formatted = split[0] + "." + split[1];
  return formatted;
}

function digitGroup(x) {
  var parts = x.toString().split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return parts.join(".");
}

function walletFormatUSD(number) {
  //Outputs a string with number formatted for wallet balance display
  //Always 2 digits after comma
  //Thousands separator if >=10,000.00000000
  if (isNaN(number)) return "?";
  var formatted = parseFloat(number);
  formatted = formatted.toFixed(2);
  var split = formatted.toString().split('.');
  if (split[0].length >= 5) {
    split[0] = split[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
  }
  formatted = split[0] + "." + split[1];
  return formatted;
}

function listAssets(){
  var dialog = bootbox.dialog({
    title: 'Portfolio',
    message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
});
dialog.init(function(){
    setTimeout(function(){
      var output = "";
      var btc_val = btc_balance * btc_usd;
      btc_val = "$" + walletFormatUSD(btc_val);
      var xcp_val = xcp_balance * xcp_usd;
      xcp_val = "$" + walletFormatUSD(xcp_val);
      output += '<div class="col-md-12">';
      output += '<div class="card">';
      output += '<div class="card-header" data-background-color="blue">';
      output += '<h4 class="title">Balances</h4>';
      output += '<p class="category">All Tokens</p>';
      output += '</div>';
      output += '<div class="card-content table-responsive">';
      output += "<table class=\"table\">";
      output += "<tr><td class=\"tbltok\">BTC</td><td class=\"tblbal\">" + bal_displ(btc_balance) + "</td><td class=\"tblusd\">" + btc_val + "</td></tr>";
      output += "<tr><td class=\"tbltok\">XCP</td><td class=\"tblbal\">" + bal_displ(xcp_balance) + "</td><td class=\"tblusd\">" + xcp_val + "</td></tr>";
      for (var i = 0; i < balances.length; i++) {
        var asset_val = "";
        for (var j = 0; j < prices.length; j++) {
          if (balances[i][0] == prices[j][0]) {
            asset_val = balances[i][1] * prices[j][1];
            asset_val = "$" + walletFormatUSD(asset_val);
          } else {
            asset_val = balances[i][3];
            asset_val = "$" + walletFormatUSD(asset_val);
          }
        }
        //output += balances[i][0] + " " + balances[i][1] + "<br>";
        output += "<tr><td class=\"tbltok\">" + name_displ(balances[i][0]) + "</td><td class=\"tblbal\">" + bal_displ(balances[i][1]) + "</td><td class=\"tblusd\">" + asset_val + "</td></tr>";
      }
      output += "</table>";
      output += '</div>';
      output += '</div>';
      output += '</div>';
        dialog.find('.bootbox-body').html(output);
    }, 1000);
});
}

function displayOrderForm() {
  var rows = document.getElementsByClassName("row");
  rows[0].innerHTML = "";
  rows[1].innerHTML = "";
  rows[2].innerHTML = "";

  document.getElementById('dashboard').classList.remove("active");
  document.getElementById('gallery').classList.remove("active");
  document.getElementById('order').classList.add("active");
  document.getElementById('myorder').classList.remove("active");
  document.getElementById('history').classList.remove("active");
  document.getElementById('transaction').classList.remove("active");
  document.getElementById('wallet').classList.remove("active");

  var security = localStorage.getItem("security");

  output = "";
  output += '<div class="col-md-12">';
  output += '<div class="card">';
  output += '<div class="card-header" data-background-color="blue">';
  output += '<h4 class="title">Create a new order</h4>';
  output += '<p class="category">IF BTC put the quantity in Satoshis (1 BTC = 100000000 Satoshis)</p>';
  output += '</div>';
  output += '<div class="card-content">';
  output += '<form>';
  output += '<div class="row">';
  output += '<div class="col-md-6">';
  output += '<div class="form-group label-floating">';
  output += '<label class="control-label">Give Asset</label>';
  output += '<input type="text" class="form-control" value="" name="give_asset" id="giveAsset" onchange="valOrder();" onkeydown="upperCaseF(this)">';
  output += '</div>';
  output += '</div>';
  output += '<div class="col-md-6">';
  output += '<div class="form-group label-floating">';
  output += '<label class="control-label">Amount</label>';
  output += '<input type="number" class="form-control" min=0 name="give_quantity" id="giveAmount" value="" onchange="valOrder();">';
  output += '</div>';
  output += '</div>';
  output += '</div>';
  output += '<div class="row">';
  output += '<div class="col-md-6">';
  output += '<div class="form-group label-floating">';
  output += '<label class="control-label">Get Asset</label>';
  output += '<input type="text" class="form-control" value="" name="get_asset" id="getAsset" onchange="valOrder();" onkeydown="upperCaseF(this)">';
  output += '</div>';
  output += '</div>';
  output += '<div class="col-md-6">';
  output += '<div class="form-group label-floating">';
  output += '<label class="control-label">Amount</label>';
  output += '<input type="number" class="form-control" name="get_quantity" id="getAmount" value="" onchange="valOrder();">';
  output += '</div>';
  output += '</div>';
  output += '</div>';
  output += '<div class="row">';
  output += '<div class="col-md-12">';
  output += '<div class="form-group label-floating">';

  if(security=='Basica'){
    output += '<label class="control-label" style="visibility:hidden;">Password</label>';
    output += '<input type="password" class="form-control" style="visibility:hidden;" id="orderPassword" value="U2VndXJpZGFkIEJhc2ljYQ==" oninput="valOrder();">';
  } else {
    output += '<label class="control-label">Password</label>';
    output += '<input type="password" class="form-control" id="orderPassword" value="U2VndXJpZGFkIEJhc2ljYQ==" oninput="valOrder();">';
  }

  output += '</div>';
  output += '</div>';
  output += '</div>';
  output += '<a class="btn btn-primary pull-right" id="orderButton" onClick="getData();">Send Order</a>';
  output += '<div class="clearfix"></div>';
  output += '</form>';
  output += '</div>';
  output += '</div>';

  var balancesRow = document.getElementsByClassName("row");
  balancesRow[1].innerHTML = output;

}

function getData(){
  var giveA = document.getElementById('giveAsset').value;
  var giveQ = document.getElementById('giveAmount').value;
  var getA = document.getElementById('getAsset').value;
  var getQ = document.getElementById('getAmount').value;

  post(address,giveA,giveQ,getA,getQ);

}

function post(address,giveA,giveQ,getA,getQ){
  console.log("Entro");
  $.post("orders.php",
{
    address: address,
    give_asset: giveA,
    give_quantity: giveQ,
    get_asset: getA,
    get_quantity:getQ
},
function(data, status){
    console.log("Data: " + data + "\nStatus: " + status);
    sendOrder(data);
});
}

function burn(address,quantity){
  console.log("Entro");
  $.post("burn.php",
{
    address: address,
    quantity: quantity
},
function(data, status){
    console.log("Data: " + data + "\nStatus: " + status);
    sendOrder(data);
});
}

function displayOpenOrderForm() {
  var rows = document.getElementsByClassName("row");
  rows[0].innerHTML = "";
  rows[1].innerHTML = "";
  rows[2].innerHTML = "";

  $.getJSON("https://testnet.xchain.io/api/orders/"+address, function(data) {
    output = '';
    for (var i = 0; i < data.total; i++) {
      if (data.data[i].status=='open') {
        output += '<div class="col-md-6 col-sm-12">';
        output += '<div class="card">';
        output += '<div class="card-content table-responsive">';
        output += '<table class="table table-hover">';
        output += '<tbody>';
        output += '<tr>';

        var valGiveText = "";

        for (var x = 0; x < tokens.length; x++) {
          if ( tokens[x] == data.data[i].give_asset ) {
            valGiveText = true;
            output += '<td><p>Selling:</p><img class="cardOrden" src="'+url_tokens[x]+'"><p>'+data.data[i].give_asset+'</p></td>';
          }
        }

        if (valGiveText!=true) {
          output += '<td><p>Selling:</p><p>'+data.data[i].give_asset+'</p></td>';
        }

        output += '<td><i class="material-icons" style="font-size: 50px;">trending_flat</i></</td>';

        var valGetText= "";

        for (var x = 0; x < tokens.length; x++) {
          if ( tokens[x] == data.data[i].get_asset ) {
            valGetText = true;
            output += '<td><p>Buying:</p><img class="cardOrden" src="'+url_tokens[x]+'"><p>'+data.data[i].get_asset+'</p></td>';
          }
        }

        if (valGetText!=true) {
          output += '<td><p>Buying:</p><p>'+data.data[i].get_asset+'</p></td>';
        }

        output += '</tr>';
        output += '<tr>';
        output += '<td>'+data.data[i].give_quantity+'</td>';
        output += '<td></td>';
        output += '<td>'+data.data[i].get_quantity+'</td>';
        output += '</tr>';
        output += '</tbody>';
        output += '</table>';
        output += '</div>';
        output += '</div>';
        output += '</div>';
      }
    }
    var balancesRow = document.getElementsByClassName("row");
    balancesRow[0].innerHTML = output;
  });

  document.getElementById('dashboard').classList.remove("active");
  document.getElementById('gallery').classList.remove("active");
  document.getElementById('history').classList.remove("active");
  document.getElementById('myorder').classList.add("active");
  document.getElementById('order').classList.remove("active");
  document.getElementById('transaction').classList.remove("active");
  document.getElementById('wallet').classList.remove("active");


  //document.getElementById('address').value = localStorage.getItem('address');
}

// *** Below functions for sending BTC, XCP or Asset
function displaySendForm() {

  var rows = document.getElementsByClassName("row");
  rows[0].innerHTML = "";
  rows[1].innerHTML = "";
  rows[2].innerHTML = "";

  document.getElementById('dashboard').classList.remove("active");
  document.getElementById('gallery').classList.remove("active");
  document.getElementById('history').classList.remove("active");
  document.getElementById('order').classList.remove("active");
  document.getElementById('myorder').classList.remove("active");
  document.getElementById('transaction').classList.add("active");
  document.getElementById('wallet').classList.remove("active");

  output = "";
  output += '<div class="col-md-12">';
  output += '<div class="card">';
  output += '<div class="card-header" data-background-color="blue">';
  output += '<h4 class="title">Send</h4>';
  output += '<p class="category"><i><span id=\"txCost\">&nbsp;</span></i></p>';
  output += '</div>';
  output += '<div class="card-content">';
  output += '<div class="row">';
  output += '<div class="col-md-6">';
  output += '<div class="form-group label-floating is-empty"><label class="control-label">Asset</label>';
  output += '<input type="text" class="form-control" id="sendAsset" value="" onchange="valSendInp();" onkeydown="upperCaseF(this)">';
  output += '<span class="material-input"></span>';
  output += '</div>';
  output += '</div>';
  output += '<div class="col-md-6">';
  output += '<div class="form-group label-floating is-empty"><label class="control-label">Amount</label>';
  output += '<input type="text" class="form-control" id="sendAmount" value="" onchange="valSendInp();">';
  output += '</div>';
  output += '</div>';
  output += '</div>';
  output += '<div class="row">';
  output += '<div class="col-md-12">';
  output += '<div class="form-group label-floating"><label class="control-label">Recipient Address</label>';
  output += '<input type="text" class="form-control" id="sendTo" onchange="valSendInp();"></div>';
  output += '</div>';
  output += '</div>';
  output += '<div class="row">';
  output += '<div class="col-md-6">';
  output += '<div class="form-group label-floating is-empty"><label class="control-label">Fee</label>';
  output += '<input type="number" class="form-control" id="sendFee" value="'+ TX_FEE.toFixed(8) +'" onchange="displayTxCost();valSendInp();" step="0.00001"></div>';
  output += '</div>';
  output += '<div class="col-md-6"><div class="form-group label-floating is-empty">';
  var security = localStorage.getItem("security");

  if(security=='Basica'){
    output += '<label class="control-label" style="visibility:hidden;">Password</label>';
    output += '<input type="password" class="form-control" style="visibility:hidden;" id="sendPassword" value="U2VndXJpZGFkIEJhc2ljYQ==" oninput="valSendInp();">';
  }else {
    output += '<label class="control-label">Password</label>';
    output += '<input type="password" class="form-control" id="sendPassword" value="" oninput="valSendInp();">';
  }

  output += '</div></div>';
  output += '</div>';
  output += '<button id="sendButton" type="button" class="btn btn-primary pull-right" onclick="sendTx();">Send Asset</button>';
  output += '<div class="clearfix"></div>';
  output += '</div>';
  output += '</div>';
  output += '</div>';

  var balancesRow = document.getElementsByClassName("row");
  balancesRow[0].innerHTML = output;
}

function displayTxCost() {
  var send_asset = document.getElementById('sendAsset').value;
  var fee = document.getElementById("sendFee").value;
  var cost_btc = Number(fee);
  if (send_asset != "BTC") cost_btc += TO_RECEIVER;
  var cost_usd = cost_btc * btc_usd;
  var output = "";
  output += "Total Cost: " + cost_btc.toFixed(8) + " BTC ($" + cost_usd.toFixed(2) + ")";
  document.getElementById('txCost').innerHTML = output;
}

function valSendInp() {
  displayTxCost();
  var color_ok = "#3e8e41";
  var color_wrong = "maroon";
  var send_asset = document.getElementById('sendAsset').value;
  var send_amount = document.getElementById('sendAmount').value;
  var send_to = document.getElementById('sendTo').value;
  var send_fee = document.getElementById('sendFee').value;
  var send_password = document.getElementById('sendPassword').value;

  /*if (typeof(pw) != 'undefined' && pw != null) {
    var pw = "U2VndXJpZGFkIEJhc2ljYQ==";
  } else {
    var pw = document.getElementById("newPassword").value;
  }*/

  var assetOK = false;
  var amountOK = false;
  var toOK = false;
  var feeOK = false;
  var passwordOK = false;
  var assetInd = -1;
  if (send_asset == "XCP") {
    assetOK = true;
  } else if (send_asset == "BTC") {
    assetOK = true;
  } else {
    for (var i = 0; i < balances.length; i++) {
      if (balances[i][0] == send_asset) {
        assetOK = true;
        assetInd = i;
      }
    }
  }
  send_amount = Number(send_amount);
  if (isNaN(send_amount)) {

  } else if (send_amount <= 0) {

  } else if (send_asset == "XCP" && send_amount <= xcp_balance) {
    amountOK = true;
  } else if (send_asset == "BTC" && send_amount <= btc_balance) {
    amountOK = true;
  } else if (send_amount <= Number(balances[assetInd][1])) {
    amountOK = true;
  }
  //address test not complete. simple test to check length is 26-34 chars
  //and starts with 1 (counterparty does not work with multisig '3' addresses)
  if (send_to.length >= 26 && send_to.length <= 34) {
    toOK = true;
  }
  send_fee = Number(send_fee);
  if (isNaN(send_fee)) {

  } else if (send_fee >= 0 && send_fee < 0.01) {
    feeOK = true; //hardcoded max 0.00999999 .. in extreme cases more may be desired
  }
  if (send_password != "") {
    var decrypted = CryptoJS.AES.decrypt(encrypted_pp, send_password);
    if (decrypted != "") {
      passwordOK = true;
    }
  }

  if (assetOK && amountOK && toOK && feeOK && passwordOK) {
    document.getElementById('sendButton').disabled = false;
  } else {
    document.getElementById('sendButton').disabled = true;
  }

  if (assetOK == true) document.getElementById('sendAsset').style.color = color_ok;
  else document.getElementById('sendAsset').style.color = color_wrong;
  if (amountOK == true) document.getElementById('sendAmount').style.color = color_ok;
  else document.getElementById('sendAmount').style.color = color_wrong;
  if (toOK == true) document.getElementById('sendTo').style.color = color_ok;
  else document.getElementById('sendTo').style.color = color_wrong;
  if (feeOK == true) document.getElementById('sendFee').style.color = color_ok;
  else document.getElementById('sendFee').style.color = color_wrong;
}

function valOrder() {
  var color_ok = "#3e8e41";
  var color_wrong = "maroon";
  var give_asset = document.getElementById('giveAsset').value;
  var give_amount = document.getElementById('giveAmount').value;
  var order_password = document.getElementById('orderPassword').value;
  var giveOK = false;
  var giveAmountOK = false;
  var orderPasswordOK = false;
  var orderInd = -1;
  if (give_asset == "XCP") {
    giveOK = true;
  } else if (give_asset == "BTC") {
    giveOK = true;
  } else {
    for (var i = 0; i < balances.length; i++) {
      if (balances[i][0] == give_asset) {
        giveOK = true;
        orderInd = i;
      }
    }
  }
  give_amount = Number(give_amount);
  if (isNaN(give_amount)) {

  } else if (give_amount <= 0) {

  } else if (give_asset == "XCP" && give_amount <= xcp_balance) {
    giveAmountOK = true;
  } else if (give_asset == "BTC" && give_amount <= btc_balance) {
    giveAmountOK = true;
  } else if (give_amount <= Number(balances[orderInd][1])) {
    giveAmountOK = true;
  }

  if (order_password != "") {
    var decrypted = CryptoJS.AES.decrypt(encrypted_pp, order_password);
    if (decrypted != "") {
      orderPasswordOK = true;
    }
  }

  if (giveOK && giveAmountOK && orderPasswordOK) {
    //document.getElementById('sendOrder').disabled = false;
  } else {
    //document.getElementById('sendOrder').disabled = true;
  }

  if (giveOK == true) document.getElementById('giveAsset').style.color = color_ok;
  else document.getElementById('giveAsset').style.color = color_wrong;
  if (giveAmountOK == true) document.getElementById('giveAmount').style.color = color_ok;
  else document.getElementById('giveAmount').style.color = color_wrong;
}

function sendTx() {

  var send_asset = document.getElementById('sendAsset').value;
  var send_amount = document.getElementById('sendAmount').value;
  var send_to = document.getElementById('sendTo').value;
  var send_fee = document.getElementById('sendFee').value;
  var send_password = document.getElementById('sendPassword').value;
  var pp = CryptoJS.AES.decrypt(encrypted_pp, send_password);
  pp = pp.toString(CryptoJS.enc.Utf8);
  for (var i = 0; i < balances.length; i++) {
    if (balances[i][0] == send_asset) {
      send_asset = balances[i][2];
    }
  }
  document.getElementById('sendButton').disabled = true;
  initializeTxPp(address, send_to, send_asset, send_amount, TO_RECEIVER, send_fee, pp)
}

function sendOrder(hex) {
  var security = localStorage.getItem("security");

  if(security=='Basica'){

    var order_password = 'U2VndXJpZGFkIEJhc2ljYQ==';

    var encrypted_pp = localStorage.getItem("encrypted_pp");
    var pp = CryptoJS.AES.decrypt(encrypted_pp, order_password);

    pp = pp.toString(CryptoJS.enc.Utf8);

    orderTxPp(address, pp, hex);
  }
  else {
    bootbox.prompt({
      title: "Confirm to signed your order",
      inputType: 'password',
      buttons: {
        cancel: {
            label: '<i class="fa fa-times"></i> Cancel'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Confirm'
        }
      },
      callback: function (result) {
        var order_password = result;

        if (result==null){
          bootbox.hideAll();
        }

        var encrypted_pp = localStorage.getItem("encrypted_pp");
        var pp = CryptoJS.AES.decrypt(encrypted_pp, order_password);

        pp = pp.toString(CryptoJS.enc.Utf8);

        orderTxPp(address, pp);
      }
    });
  }
}

function displayGallery(){
  var rows = document.getElementsByClassName("row");
  rows[0].innerHTML = "";
  rows[1].innerHTML = "";
  rows[2].innerHTML = "";

  document.getElementById('dashboard').classList.remove("active");
  document.getElementById('gallery').classList.add("active");
  document.getElementById('order').classList.remove("active");
  document.getElementById('history').classList.remove("active");
  document.getElementById('myorder').classList.remove("active");
  document.getElementById('transaction').classList.remove("active");
  document.getElementById('wallet').classList.remove("active");
  var output = "";

  output += '<div class="row">';
  output += '<div class="col-sm-2" ></div>';
  output += '<div class="col-sm-8" id="contentInfo" >';
  output += '</div>';
  output += '<div class="col-sm-2"></div>';
  output += '</div>';

  var informationRow = document.getElementsByClassName("row");
  informationRow[0].innerHTML = output;


    for (var i = 0; i < tokens.length; i++) {
      (function(i) { // protects i in an immediately called function
        $.getJSON('https://testnet.xchain.io/api/market/'+tokens[i]+'/OLINCOIN/orderbook', function (data) {


          if (data.asks != undefined && data.asks.length>=1) {
            var value = url_tokens[i];

            output += '<div class="row" style="background: white; margin-bottom: 40px; box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14); border-radius: 3px;">';
            output += '<div class="col-sm-4">';
            output += '<h2 class="category tbltok" style=" text-align: center; margin-top: 22px;">'+tokens[i]+'</h2>';
            output += '<img class="img-fluid" style="max-width:100%" src="'+value+'"></div>';
            output += '<div class="col-sm-8">';
            output += '<div id="listAsk-'+tokens[i]+'">';
            output += '</div>';

            output += '</div>';
            output += '</div>';

            var informationRow = document.getElementById("contentInfo");
            informationRow.innerHTML = output;
          }
        });
      })(i);
    }
    setTimeout(uploadTable, 1500);
}

function uploadTable(){
  for (var i = 0; i < tokens.length; i++) {
    (function(i) { // protects i in an immediately called function

      $.getJSON('https://testnet.xchain.io/api/market/'+tokens[i]+'/OLINCOIN/orderbook', function (data) {

        var out = "" ;

        if (data.asks != undefined && data.asks.length>=1) {
          out += '<table class="table" style="table-layout: fixed; text-align:center;">';
          out += '<tbody>';
          out += '<tr>';
          out += '<td>Quantity</td>';
          out += '<td>P/U</td>';
          out += '</tr>';
          out += '<tr>';
          out += '<td class="tbltok"><input id="id-'+tokens[i]+'" type="number" min="1" style="text-align: center;"></td>';
          out += '<td class="tbltok">';
          out += '<div class="mui-select"><select id="select-'+tokens[i]+'" onchange="checkSelect(\''+tokens[i]+'\')" >';
          out += '<option value="" selected></option>';

          for (var y = 0; y < data.asks.length; y++) {
            var num = parseInt(data.asks[y][0]);
            out += '<option value="'+data.asks[y][1]+'" >'+num.toFixed(2)+'</option>';
          }

          out += '</select>';

          out += '<div class="mui-event-trigger"></div>';
          out += '</div>';
          out += '</td>';
          out += '</tr>';
          out += '</tbody>';
          out += '</table>';

          out += '<div class="col-md-12">';
          out += "<a class=\"btn btn-primary btn-block\" onClick=\"buyImage('"+  tokens[i] + "')\">BUY</a>";
          out += '</div>';

          document.getElementById( 'listAsk-' + tokens[i] ).innerHTML = out;
        }
      });
    })(i);
  }
}

function checkSelect(token){
  var element = document.getElementById("select-"+token);

  var campo2 = document.getElementById('id-'+token);

  var selectInd = document.getElementById("select-"+token);

  var maxQ = selectInd.options[selectInd.selectedIndex].value;


  campo2.value = maxQ;

  $("#id-"+token).attr({
     "max" : maxQ,
     "min" : 1
  });

  }

// *** Below functions show passphrase, address, key, etc
function displayWalletInfo() {
  var rows = document.getElementsByClassName("row");
  rows[0].innerHTML = "";
  rows[1].innerHTML = "";
  rows[2].innerHTML = "";

  document.getElementById('dashboard').classList.remove("active");
  document.getElementById('gallery').classList.remove("active");
  document.getElementById('history').classList.remove("active");
  document.getElementById('order').classList.remove("active");
  document.getElementById('myorder').classList.remove("active");
  document.getElementById('transaction').classList.remove("active");
  document.getElementById('wallet').classList.add("active");

  var security = localStorage.getItem("security");

  if(security=='Basica'){
    genWalletDetails('U2VndXJpZGFkIEJhc2ljYQ==');
  } else {
    bootbox.prompt({
      title: "Enter your password",
      inputType: 'password',
      buttons: {
        cancel: {
            label: '<i class="fa fa-times"></i> Cancel'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Confirm'
        }
      },
      callback: function (result) {
        if (result==null){
          bootbox.hideAll();
          displayWallet();
        } else {
          genWalletDetails(result);
        }
      }
    });
  }
}

function genWalletDetails(pw) {
  var decrypted = "";
  var decrypted = CryptoJS.AES.decrypt(encrypted_pp, pw);
  decrypted = decrypted.toString(CryptoJS.enc.Utf8);
  if (decrypted.length <= 2) return;
  var addr_list = genAddr(decrypted, NEW_ADDRESS_INDEX + 5, true);
  var current_addr = addr_list[NEW_ADDRESS_INDEX][0];
  var current_privkey = addr_list[NEW_ADDRESS_INDEX][1];
  var output = "";
  output +='<div class="col-md-12">';
  output +='<div class="card">';
  output += '<div class="card-header" data-background-color="blue">';
  output += '<h4 class="title">Wallet Info</h4>';
  output += '<p class="category"></p>';
  output += '</div>';
  output += '<div class="card-content" style="word-wrap: break-word;">';
  output += "<b>Passphrase</b><br>";
  output += decrypted + "<br><br>";
  output += "<b>Address</b><br>";
  output += current_addr + "<br><br>";
  output += "<b>Private Key</b><br>";
  output += current_privkey + "<br><br>";
  output += "From the same passphrase you have<br>";
  output += "access to a list of addresses:<br>";
  output += "<br><code style=\"display:block;white-space:pre-wrap;\">";
  for (var i = 0; i < addr_list.length; i++) {
    output += "#" + i + " ";
    output += addr_list[i][0] + "\n";
    output += addr_list[i][1] + "\n";
    if (i < addr_list.length - 1) output += "\n";
  }
  output += "</code><br>";
  output += '</div>';
  output += '</div>';
  output += '</div>';

  var informationRow = document.getElementsByClassName("row");
  informationRow[0].innerHTML = output;
}

function instructClearMemory() {
  if (document.getElementById("checkNewWallet1").checked && document.getElementById("checkNewWallet2").checked && document.getElementById("checkNewWallet3").checked && document.getElementById("checkNewWallet4").checked) {
    var output = "";
    output += "<br>Do the following to delete this wallet and make a new wallet:";
    output += "<ul>";
    if (MY_ENCRYPTED_PP != "") {
      output += "<li>Set <code>MY_ENCRYPTED_PP</code> and <code>MY_ADDRESS</code> in <code>index.html</code> to empty strings</li>";
    }
    output += "<li onClick=\"localStorage.clear();displaySettings();\">CLICK ON THIS TEXT TO CLEAR MEMORY AND RESET WALLET</li>";
    document.getElementById('clearMemoryInst').innerHTML = output;
  }
}

// *** Below functions for generating new wallet
function displaySettings() {
  var output = "";
  swal({
    type: 'info',
    title: 'Take it seriously!',
    html: '<p>A password most be created. You need to write it down and keep it safe. If you lose this password, you wont be able to process any transaction in your wallet. You can choose to create your own or the server will automatucally create one for you . We do not store your password and cannot recover it if lost.<p>',
  });

  notifications.showNotification('top','left', 'A NEW WALLET HAS BEEN CREATED', 1);

  output += "<h2>IMPORTANT</h2>";
  output += "<p>These is your twelve-word passphrase. Write it down in a piece of paper and keep it safe. If you lose these passphrase you will lose access to your wallet forever. We don't store your passphrase and cannot recover it if lost. Keeping your passphrase safe is your responsability.</p><br><br>";
  output += "<h3>Address</h3>";
  output += "<span id=\"addrPreview\">&nbsp;</span><br><br>";
  output += "<h3>Passphrase</h3>";
  output += "<textarea type=\"text\" style=\"word-wrap: break-word;min-width:250px;min-height:90px;\" id=\"newPassphrase\" onblur=\"previewAddr();\" readonly=\"readonly\">"+newPassphrase()+"</textarea>";
  output += '<div class="checkbox">';
  output += '<label>';
  output += '<input name="optionsCheckboxes" type="checkbox" id="nip" onclick="visibleNIP()"><span class="checkbox-material" style="color:black;">NIP</span>';
  output += '</label>';
  output += '</div>';
  output += '<div class="validate-input" id="formNip" style="display: none;" data-validate="Enter password that must be entered every time you send">';
  output += '<div class="form-group label-floating">';
  output += '<label class="control-label"><i class="fas fa-lock fa-lg"></i></label>';
  output += '<input id="newPassword" placeholder="Session NIP" pattern=".{6,}" required="" type="password" class="form-control">';
  output += '</div>';
  output += '</div>';
  output += "<button type=\"button\" id=\"newWallet\" class=\"btn btn-primary btn-block\" onClick=\"genNewWallet();\">Open Wallet</button>";
  output += "<div id=\"walletInfo\"></div>";

  document.getElementById('container_new').innerHTML = output;
  previewAddr();
}

function previewAddr() {
  var pp = document.getElementById("newPassphrase").value;
  var ppClean = cleanPPformat(pp);
  if (ppClean != pp) document.getElementById('newPassphrase').value = ppClean;
  var address = "-";
  if (isValidPP(ppClean)) {
    var addr_list = genAddr(ppClean, NEW_ADDRESS_INDEX + 1);
    address = addr_list[NEW_ADDRESS_INDEX][0];
  }
  document.getElementById('addrPreview').innerHTML = address;
}

function genNewWallet() {
  var pp = document.getElementById("newPassphrase").value;
  var pw = document.getElementById("newPassword").value;

  if (typeof(pw) == 'undefined' || pw == null || pw == "") {
    var pw = "U2VndXJpZGFkIEJhc2ljYQ==";
    localStorage.setItem("security", "Basica");
  } else {
    localStorage.setItem("security", "Avanzada");
  }

  var encrypted = CryptoJS.AES.encrypt(pp, pw);
  var addr_list = genAddr(pp, NEW_ADDRESS_INDEX + 1);
  var addr = addr_list[NEW_ADDRESS_INDEX][0];
  localStorage.setItem("encrypted_pp", encrypted);
  localStorage.setItem("address", addr);

  //localStorage.setItem("encrypted_pp_old", encrypted_pp);
  //localStorage.setItem("address_old", address);
  var output = "<br><br>";
  output += "SUCCESS!<br><br>";
  output += "Wallet is ready to use.<br><br>";
  output += "It is permanently stored in the browser's memory,<br>";
  output += "but you must <span style=\"color:red;font-weight:bold;\">write down the 12 word passphrase</span> to recover the wallet.<br><br>";
  output += "Write down the passphrase now:<br>";
  output += "<b><mark>" + pp + "</mark></b><br><br>";
  output += "<i>OPTIONAL: To use the wallet across browsers, save below variables in index.html</i><br>";
  output += "<pre>MY_ENCRYPTED_PP = \"" + encrypted + "\";</pre>";
  output += "<pre>MY_ADDRESS = \"" + addr + "\";</pre><br>";
  output += "<button type=\"button\" id=\"openWallet\" onClick=\"location.reload();\">OPEN WALLET</button>";
  prepareWallet();
  document.getElementById('walletInfo').innerHTML = output;
}

// *** First check whether wallet exists or generate a new one
function prepareWallet() {

  if (MY_ENCRYPTED_PP != "") {
    encrypted_pp = MY_ENCRYPTED_PP;
    address = MY_ADDRESS;
    var login = document.getElementById("container");
    login.classList.add("hide");
    var dashboard = document.getElementById("wrapper");
    dashboard.classList.remove("hide");
    displayWallet();
  } else if (typeof(Storage) !== "undefined") {
    if (localStorage.encrypted_pp) {
      //Display existing wallet
      encrypted_pp = localStorage.getItem("encrypted_pp");
      address = localStorage.getItem("address");
      var login = document.getElementById("container");
      login.classList.add("hide");
      var dashboard = document.getElementById("wrapper");
      dashboard.classList.remove("hide");
      displayWallet();

    } else {
      // Prepare new wallet
      // displaySettings();
      // document.getElementById("container").innerHTML = "Generate new wallet " + newPassphrase();
      // localStorage.encrypted = 1;
    }
  } else {
    document.getElementById("container").innerHTML = "Sorry, your browser does not support web storage. Cannot use wallet.";
  }
}

function countWords(str) {
  return str.trim().split(/\s+/).length;
}

function login() {
  var pp = document.getElementById("newPassphrase");
  var pw = document.getElementById("newPassword");
  var nip = document.getElementById("nip");

  if (nip.checked == false) {
    if (typeof(pp) != 'undefined' && pp != null ) {
      if (pp.value != '' && countWords(pp.value) == 12) {
        genNewWallet();
      }
    }  } else {
    if (typeof(pp) != 'undefined' && pp != null && typeof(pw) != 'undefined' && pw != null) {
      if (pp.value != '' && countWords(pp.value) == 12 && pw.value.length > 6) {
        genNewWallet();
      }
    }
  }
}

function imageOrder(token){

  var indToken;
  var limitQuantity;

  for (var x = 0; x < balances.length; x++) {
    if ( token == name_displ(balances[x][0]) ) {
      indToken = x;
      limitQuantity = balances[x][1];
    }
  }

  var unit = bootbox.prompt({
    title: "Unitary Value in OLINCOIN",
    inputType: 'number',
    backdrop: true,
    buttons: {
      cancel: {
          label: '<i class="fa fa-times"></i> Cancel'
      },
      confirm: {
          label: '<i class="fa fa-check"></i> Confirm'
      }
    },
    callback: function (result) {

      if (result!=null){

      var unityValue = parseInt(result);
      var amountInput = bootbox.prompt({
        title: "Amount to sell",
        inputType: 'number',
        callback: function (result) {

          var amount = parseInt(result);

          var get = (unityValue*amount);

          post(address,token,amount,"OLINCOIN", get);

      }
      });

      amountInput.init(function(){
        $(".bootbox-input-number").attr({
           "max" : limitQuantity,
           "min" : 1
        });});
      }
    }
  });

  unit.init(function(){
    $(".bootbox-input-number").attr({
       "min" : 1          // values (or variables) here
    });});
}

function buyImage(token){
  var getQ = document.getElementById("id-"+token).value;
  var numInd = document.getElementById("select-"+token).selectedIndex;
  var giveQ =document.getElementById("select-"+token).options[numInd].text;

  giveQ = parseInt(giveQ);
  getQ = parseInt(getQ);

  post(address,"OLINCOIN",(giveQ*getQ),token,getQ);
}

function giveImage(token) {
  var security = localStorage.getItem("security");

  if(security=='Basica'){
    swal({
      html:'<label class="control-label">Asset</label><input type="text" class="form-control" id="sendAsset" value="'+token+'" onchange="valSendInp();" readonly><label class="control-label">Amount</label><input type="text" class="form-control" id="sendAmount" value="" onchange="valSendInp();"><label class="control-label">Recipient Address</label><input type="text" class="form-control" id="sendTo" onchange="valSendInp();"><label class="control-label">Fee</label><input type="number" class="form-control" id="sendFee" value="'+TX_FEE.toFixed(8)+'" onchange="displayTxCost();valSendInp();" step="0.00001"><label class="control-label" style="visibility:hidden;">Password</label><input type="password" class="form-control" style="visibility:hidden;" id="sendPassword" value="U2VndXJpZGFkIEJhc2ljYQ==" oninput="valSendInp();"><p id="txCost"></p><a id="sendButton" type="button" class="btn btn-primary pull-right" onclick="sendTx(); swal.close()">Send Asset</a>',
      showConfirmButton: false});
  } else {
    swal({
      html:'<label class="control-label">Asset</label><input type="text" class="form-control" id="sendAsset" value="'+token+'" onchange="valSendInp();" readonly><label class="control-label">Amount</label><input type="text" class="form-control" id="sendAmount" value="" onchange="valSendInp();"><label class="control-label">Recipient Address</label><input type="text" class="form-control" id="sendTo" onchange="valSendInp();"><label class="control-label">Fee</label><input type="number" class="form-control" id="sendFee" value="'+TX_FEE.toFixed(8)+'" onchange="displayTxCost();valSendInp();" step="0.00001"><label class="control-label">Password</label><input type="password" class="form-control" id="sendPassword" value="" oninput="valSendInp();"><p id="txCost"></p><a id="sendButton" type="button" class="btn btn-primary pull-right" onclick="sendTx(); swal.close()">Send Asset</a>',
      showConfirmButton: false});
  }

}

function detailImage(token) {
  swal({title: token,
        html:'<button type="button" class="btn btn-primary btn-block" >BUY</button>',
        showConfirmButton: false
      });
}

function convert(timestamp){

 // Unixtimestamp
 var unixtimestamp = timestamp;

 // Months array
 var months_arr = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

 // Convert timestamp to milliseconds
 var date = new Date(unixtimestamp*1000);

 // Year
 var year = date.getFullYear();

 // Month
 var month = months_arr[date.getMonth()];

 // Day
 var day = date.getDate();

 // Hours
 var hours = date.getHours();

 // Minutes
 var minutes = "0" + date.getMinutes();

 // Seconds
 var seconds = "0" + date.getSeconds();

 // Display date time in MM-dd-yyyy h:m:s format
 var convdataTime = month+'-'+day+'-'+year+' '+hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

 return convdataTime;
}

function qrAddress(){
  var dialog = bootbox.dialog({
    title: 'Receive Address',
    message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
  });
  dialog.init(function(){
    setTimeout(function(){
      output ="";
      output += "<div id='qrcode' style=\"text-align: center;\"><p>"+address+"</p><br></div>";
      dialog.find('.bootbox-body').html(output);

      $('#qrcode').qrcode(address);
    }, 1000);
  });
}
/*
function orderBook(token, callback){
    $.getJSON( "https://testnet.xchain.io/api/market/"+token+"/OLINCOIN/orderbook", function( data ) {
      callback(data);
    });
 }*/

function history(callback){
    $.getJSON( "https://testnet.xchain.io/api/history/" + address , function( data ) {
       callback(data);
    });
 }

function displayHistory(){
  var rows = document.getElementsByClassName("row");
  rows[0].innerHTML = "";
  rows[1].innerHTML = "";
  rows[2].innerHTML = "";

  document.getElementById('history').classList.add("active");
  document.getElementById('gallery').classList.remove("active");
  document.getElementById('dashboard').classList.remove("active");
  document.getElementById('order').classList.remove("active");
  document.getElementById('myorder').classList.remove("active");
  document.getElementById('transaction').classList.remove("active");
  document.getElementById('wallet').classList.remove("active");

  history(function(data){
    var output = "";
    output += '<div class="col-md-12">';
    output += '<div class="card">';
    output += '<div class="card-header" data-background-color="blue">';
    output += '<h4 class="title">All transactions</h4>';
    output += '</div>';
    output += '<div class="card-content table-responsive">';
    output += '<table class="table">';
    output += '<thead class="text-primary">';
    output += '<th>Block</th>';
    output += '<th>Status</th>';
    output += '<th>Date</th>';
    output += '<th>Get asset</th>';
    output += '<th>Give asset</th>';
    output += '<th>Type</th>';
    output += '</thead>';
    output += '<tbody>';

    $.each(data.data, function(key, val) {
      var block_index = val.block_index;
      var status = val.status;
      var timestamp = convert(val.timestamp);
      var get_asset = val.get_asset;
      var give_asset = val.give_asset;
      var tx_type = val.tx_type;
      var assetHis = val.asset;
      var destination = val.destination;
      var sourceHis = val.source;
      var quantityHis = val.quantity;

      quantityHis = parseFloat(quantityHis).toFixed(2);

      if( assetHis!=undefined ){
        if (destination == address) {
          get_asset = quantityHis + ' - ' + assetHis;
          give_asset = '';
        } else if (sourceHis == address) {
          get_asset = '';
          give_asset = quantityHis + ' - ' + assetHis;
        }
      }

      if(get_asset!=undefined){
        output += '<tr>';
        output += '<td>'+block_index+'</td>';
        output += '<td>'+status+'</td>';
        output += '<td>'+timestamp+'</td>';
        output += '<td>'+get_asset+'</td>';
        output += '<td>'+give_asset+'</td>';
        output += '<td>'+tx_type+'</td>';
        output += '</tr>';
    }
    });

    output += '</tbody>';
    output += '</table>';
    output += '</div>';
    output += '</div>';
    output += '</div>';

    var informationRow = document.getElementsByClassName("row");
    informationRow[0].innerHTML = output;
  });
}

function visibleNIP(){
  // Get the DOM reference
var contentId = document.getElementById("formNip");
// Toggle
contentId.style.display == "block" ? contentId.style.display = "none" :
contentId.style.display = "block";
}
