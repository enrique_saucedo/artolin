/* globals async */ // async binds itself to window

var bitcore = require('bitcore-lib');
var bitcoreMessage = require('bitcore-message'); // this also binds itself to bitcore.Message as soon as it's require'd


// TODO: rename to be more generic

var CWBitcore = new function() {

  this.prepareSignedTransaction = function(unsignedTx) {
      var txobj = {};
      txobj.version    = unsignedTx.version;
      txobj.lock_time  = unsignedTx.lock_time;
      txobj.ins  = [];
      for (var i=0; i < unsignedTx.ins.length; i++) {
          txobj.ins.push({
              s: bitcore.util.EMPTY_BUFFER,
              q: unsignedTx.ins[i].q,
              o: unsignedTx.ins[i].o
          });
      }
      txobj.outs = unsignedTx.outs;
      return new bitcore.Transaction(txobj);
  }

  this.signRawTransaction = function(unsignedHex, address, key) {

      // function used to each for each type
      var fnToSign = {};
      fnToSign[bitcore.Script.TX_PUBKEYHASH] = bitcore.TransactionBuilder.prototype._signPubKeyHash;
      fnToSign[bitcore.Script.TX_PUBKEY]     = bitcore.TransactionBuilder.prototype._signPubKey;
      fnToSign[bitcore.Script.TX_MULTISIG]   = bitcore.TransactionBuilder.prototype._signMultiSig;
      fnToSign[bitcore.Script.TX_SCRIPTHASH] = bitcore.TransactionBuilder.prototype._signScriptHash;

      // build key map
      var wkMap = {};
      wkMap[address] = new bitcore.WalletKey({network:NETWORK_CONF, privKey:key.eckey});

      // unserialize raw transaction
      var raw = new bitcore.buffertools.Buffer(unsignedHex, 'hex');
      var unsignedTx = new bitcore.Transaction();
      unsignedTx.parse(raw);

      // prepare  signed transaction
      var signedTx = new bitcore.TransactionBuilder();
      signedTx.tx = CWBitcore.prepareSignedTransaction(unsignedTx);

      for (var i=0; i < unsignedTx.ins.length; i++) {

          // init parameters
          var txin = unsignedTx.ins[i];
          var scriptPubKey = new bitcore.Script(txin.s);
          var input = {
              address: address,
              scriptPubKey: scriptPubKey,
              scriptType: scriptPubKey.classify(),
              i: i
          };

          // generating hash for signature
          var txSigHash = unsignedTx.hashForSignature(scriptPubKey, i, bitcore.Transaction.SIGHASH_ALL);

          // sign hash
          var ret = fnToSign[input.scriptType].call(signedTx, wkMap, input, txSigHash);

          // inject signed script in transaction object
          if (ret && ret.script) {
            signedTx.tx.ins[i].s = ret.script;
            if (ret.inputFullySigned) signedTx.inputsSigned++;
            if (ret.signaturesAdded) signedTx.signaturesAdded += ret.signaturesAdded;
          }

       }
       return signedTx.tx.serialize().toString('hex');
  }

}
