type = ['', 'info', 'success', 'warning', 'danger'];


notifications = {
    showNotification: function(from, align, text, type_id) {

      var icon_text = "";

      switch(type_id) {
          case 1:
              icon_text="info_outline";
              break;
          case 2:
              icon_text="done";
              break;
          case 3:
              icon_text="warning";
              break;
          case 4:
              icon_text="mood_bad";
              break;
          default:
              icon_text="announcement";
      }

        $.notify({
            icon: icon_text,
            message: text
        }, {
            type: type[type_id],
            timer: 4000,
            placement: {
                from: from,
                align: align
            }
        });
    }
}
