<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
 ?>
<html>
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <title>ArtOlin Wallet</title>

    <link rel="icon" type="image/png" href="assets/img/icons/favicon.png" />

    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="assets/css/material-dashboard.css?v=1.2.0" rel="stylesheet" />
    <!--  Custom CSS    -->
    <link href="assets/css/main.css" rel="stylesheet" />
    <link href="assets/css/util.css" rel="stylesheet" />
    <!--  Modules CSS   -->
    <link rel="stylesheet" type="text/css" href="assets/fonts/iconic/css/material-design-iconic-font.min.css">
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script defer src="assets/js/fontawesome-all.js"></script>
    <!--   Core JS Files   -->
    <script src="assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/material.min.js" type="text/javascript"></script>
    <!--  Custom JS Files -->
    <script src="assets/js/main.js"></script>
    <script src="assets/js/functions.js"></script>
    <script>
      window.open = function() {};
      window.alert = function() {};
      window.print = function() {};
      window.prompt = function() {};
      window.confirm = function() {};
    </script>
    <!--  Modules JS -->
    <script src="assets/js/bootbox.min.js"></script>
    <script src="assets/js/sweetalert2.all.js"></script>
    <!--  Charts Plugin -->
    <!--  Dynamic Elements plugin -->
    <!--  PerfectScrollbar Library -->
    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    <!--  CryptoLibraries    -->
    <script src="assets/js/bitcore.min.js"></script>
    <script src="assets/js/seedrandom.min.js">
      //https://github.com/davidbau/seedrandom
    </script>
    <script src="assets/js/passphrase.js"></script>
    <script src="assets/js/crypto-js-aes.js">
      //https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js
    </script>
    <!--  XCP_Tools    -->
    <script src="assets/js/xcp-wallet/mnemonic.js">
      //https://github.com/loon3/XCP-Wallet
    </script>
    <script src="assets/js/xcp-wallet/utxo.js"></script>
    <script src="assets/js/xcp-wallet/transactions.js"></script>
    <script src="assets/js/xcp-wallet/rc4.js"></script>
    <script src="assets/js/xcp-wallet/convert-type.js"></script>
    <script src="assets/js/xcp-wallet/decode.js"></script>
    <script src="assets/js/xcp-wallet/biginteger.js"></script>
    <!--  Coinb    -->
    <script type="text/javascript" src="assets/js/coinb/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="assets/js/coinb/moment.min.js"></script>
    <script type="text/javascript" src="assets/js/coinb/crypto-min.js"></script>
    <script type="text/javascript" src="assets/js/coinb/crypto-sha256.js"></script>
    <script type="text/javascript" src="assets/js/coinb/crypto-sha256-hmac.js"></script>
    <script type="text/javascript" src="assets/js/coinb/sha512.js"></script>
    <script type="text/javascript" src="assets/js/coinb/ripemd160.js"></script>
    <script type="text/javascript" src="assets/js/coinb/aes.js"></script>
    <script type="text/javascript" src="assets/js/coinb/qrcode.js"></script>
    <script type="text/javascript" src="assets/js/coinb/qcode-decoder.min.js"></script>
    <script type="text/javascript" src="assets/js/coinb/jsbn.js"></script>
    <script type="text/javascript" src="assets/js/coinb/ellipticcurve.js"></script>
    <script type="text/javascript" src="assets/js/coinb/coin.js"></script>
    <script type="text/javascript" src="assets/js/coinb/coinbin.js"></script>
    <!-- Material Dashboard javascript methods -->
    <script src="assets/js/material-dashboard.js?v=1.2.0"></script>
    <script src="assets/js/notification.js"></script>
    <!-- load MUI -->
    <link href="//cdn.muicss.com/mui-0.9.39/css/mui.min.css" rel="stylesheet" type="text/css" />
    <script src="//cdn.muicss.com/mui-0.9.39/js/mui.min.js"></script>
    <!--Introduce variables -->
    <script type="text/javascript" src="assets/js/jquery.qrcode.min.js"></script>
    <script>
      //WALLET INFO
      //Leave blank to use browser' local storage instead
      MY_ENCRYPTED_PP = "";
      MY_ADDRESS = "";
      //Parameters
      NEW_ADDRESS_INDEX = 0; //when generating new wallet. 0 is 1st address from passphrase
      TX_FEE = 0.0003; //default transaction fee in BTC
      TO_RECEIVER = 0.00005420; //BTC dust sent along asset
    </script>
    <script>
      var encrypted_pp = "";
      var address = "";
      var btc_balance = "?";
      var tokens = [];
      var url_tokens = [];
      var xcp_balance = "?";
      var balances = [];
      var btc_usd = "?";
      var xcp_usd = "?";
      var xcp_btc = "?";
      var olin_usd = "?";
      var prices = [];

      $.getJSON("https://www.artolin.org/json/artlist.json", function(data) {
        $.each( data, function( key, val ) {
          tokens.push(key);
          url_tokens.push(val);
        });
      });
    </script>
  </head>
  <body onload="prepareWallet();">
    <div id="container"></div>
  </body>
</html>
