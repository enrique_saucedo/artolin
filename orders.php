<?php

require 'php/includes/Client.php';
use JsonRPC\Client;

include("php/settings.php");

error_reporting(E_ALL);
ini_set('display_errors', 1);

$address = ((isset($_POST['address'])) ? ($_POST['address']) : (''));
$give_asset = ((isset($_POST['give_asset'])) ? ($_POST['give_asset']) : (''));
$give_quantity = ((isset($_POST['give_quantity'])) ? ($_POST['give_quantity']) : (''));
$get_asset = ((isset($_POST['get_asset'])) ? ($_POST['get_asset']) : (''));
$get_quantity = ((isset($_POST['get_quantity'])) ? ($_POST['get_quantity']) : (0));

if(isset($get_asset) && $get_asset=='XCP'){
  $get_quantity = (int)$_POST["get_quantity"]*100000000;
} else if(isset($get_asset) && $get_asset=='OLINCOIN'){
  $get_quantity = (int)$_POST["get_quantity"];
} else {
  $get_quantity = (int)$_POST["get_quantity"];
}

if(isset($give_asset) && $give_asset=='XCP'){
  $give_quantity = (int)$_POST["give_quantity"]*100000000;
} else if(isset($give_asset) && $give_asset=='OLINCOIN'){
  $give_quantity = (int)$_POST["give_quantity"];
} else {
  $give_quantity = (int)$_POST["give_quantity"];
}

  if (isset($give_asset) && $give_asset != "" && isset($give_quantity) && $give_quantity != "" && isset($get_asset) && $get_asset != "" && isset($get_quantity) && $get_quantity != "") {
    $client = new Client($cp_server);
    $client->authentication($cp_user, $cp_password);

    $result = $client->execute('create_order', array('source' => (string)$address, 'give_asset' => (string)$give_asset, 'give_quantity' => (int)$give_quantity, 'get_asset' => (string)$get_asset, 'get_quantity' => (int)$get_quantity, 'fee_required' => 0, 'expiration' => 360));

    echo $result;
	}
    else {
			echo 'Error';
    }
?>
