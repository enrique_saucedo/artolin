<?php

require 'php/includes/Client.php';
use JsonRPC\Client;

include("php/settings.php");

error_reporting(E_ALL);
ini_set('display_errors', 1);

$address = ((isset($_POST['address'])) ? ($_POST['address']) : (''));
$quantity = ((isset($_POST['quantity'])) ? ($_POST['quantity']) : (''));

$quantity = (float)$_POST["quantity"]*100000000;

$client = new Client($cp_server);
$client->authentication($cp_user, $cp_password);

$result = $client->execute('create_burn', array('source' => (string)$address, 'quantity' => $quantity));

echo $result;

?>
