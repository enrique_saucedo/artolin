const newWallet = ({ newPassphrase }) => `
  <h2>IMPORTANT</h2>
  <p>These is your twelve-word passphrase.
  Write it down in a piece of paper and keep it safe.
  If you lose these passphrase you will lose access to your wallet forever.
  We don't store your passphrase and cannot recover it if lost.
  Keeping your passphrase safe is your responsability.</p>
  <br><br>
  <h3>Address</h3>
  <span id="addrPreview">&nbsp;</span><br><br>
  <h3>Passphrase</h3>
  <textarea type="text" style="word-wrap: break-word; min-width:250px; min-height:90px;" id="newPassphrase" onblur="previewAddr();" readonly="readonly">${newPassphrase}</textarea>
  <div class="checkbox">
      <label>
          <input name="optionsCheckboxes" id="nip" onclick="visibleNIP()" type="checkbox"><span class="checkbox-material"><span class="check"></span></span><span class="checkbox-material" style="color:black;">NIP</span>
      </label>
  </div><br>
  <div class="validate-input" id="formNip" style="display: none;" data-validate="Enter password that must be entered every time you send">
    <div class="form-group label-floating">
      <label class="control-label"><i class="fas fa-lock fa-lg"></i></label>
      <input id="newPassword" placeholder="Session NIP" pattern=".{6,}" required="" type="password" class="form-control">
    </div>
  </div>
  <button type="button" id="newWallet" class="btn btn-primary btn-block" onClick="genNewWallet();">Open Wallet</button>
  <div id="walletInfo"></div>`;
