function prepareWallet() {
  if (MY_ENCRYPTED_PP != "") {
    encrypted_pp = MY_ENCRYPTED_PP;
    address = MY_ADDRESS;

    $(function(){
      $("#container").load("../html/dashboard.html", function() { displayWallet(); });
    });

  } else if (typeof(Storage) !== "undefined") {
    if (localStorage.encrypted_pp) {
      encrypted_pp = localStorage.getItem("encrypted_pp");
      address = localStorage.getItem("address");

      $(function(){
        $("#container").load("../html/dashboard.html", function() { displayWallet(); });
      });

    } else {}
  } else {
    $(function(){
      $("#container").load("../html/dashboard.html", function() { displayWallet(); });
    });  }
}

function countWords(str) {
  return str.trim().split(/\s+/).length;
}

function visibleNIP(){
  var contentId = document.getElementById("formNip");

  contentId.style.display == "block" ? contentId.style.display = "none" :
  contentId.style.display = "block";
}

function login() {
  var pp = document.getElementById("newPassphrase");
  var pw = document.getElementById("newPassword");
  var nip = document.getElementById("nip");

  if (nip.checked == false) {
    if (typeof(pp) != 'undefined' && pp != null ) {
      if (pp.value != '' && countWords(pp.value) == 12) {
        genNewWallet();
      }
    }  } else {
    if (typeof(pp) != 'undefined' && pp != null && typeof(pw) != 'undefined' && pw != null) {
      if (pp.value != '' && countWords(pp.value) == 12 && pw.value.length > 6) {
        genNewWallet();
      }
    }
  }
}

function genNewWallet() {
  var pp = document.getElementById("newPassphrase").value;
  var pw = document.getElementById("newPassword").value;

  if (typeof(pw) == 'undefined' || pw == null || pw == "") {
    var pw = "U2VndXJpZGFkIEJhc2ljYQ==";
    localStorage.setItem("security", "Basica");
  } else {
    localStorage.setItem("security", "Avanzada");
  }

  var encrypted = CryptoJS.AES.encrypt(pp, pw);
  var addr_list = genAddr(pp, NEW_ADDRESS_INDEX + 1);
  var addr = addr_list[NEW_ADDRESS_INDEX][0];
  localStorage.setItem("encrypted_pp", encrypted);
  localStorage.setItem("address", addr);

  prepareWallet();
}

function displaySettings() {
  var output = "";
  swal({
    type: 'info',
    title: 'Take it seriously!',
    html: '<p>A password most be created. You need to write it down and keep it safe. If you lose this password, you wont be able to process any transaction in your wallet. You can choose to create your own or the server will automatucally create one for you . We do not store your password and cannot recover it if lost.<p>',
  });

  notifications.showNotification('top','left', 'A NEW WALLET HAS BEEN CREATED', 1);

  var newPass = newPassphrase();

  $('#container_new').html([{ newPassphrase : newPass },].map(newWallet).join(''));

  previewAddr();
}

function previewAddr() {
  var pp = document.getElementById("newPassphrase").value;
  var ppClean = cleanPPformat(pp);

  if (ppClean != pp) document.getElementById('newPassphrase').value = ppClean;

  var address = "-";

  if (isValidPP(ppClean)) {
    var addr_list = genAddr(ppClean, NEW_ADDRESS_INDEX + 1);
    address = addr_list[NEW_ADDRESS_INDEX][0];
  }

  document.getElementById('addrPreview').innerHTML = address;
}

function displayWallet() {

  var rows = document.getElementsByClassName("row");

  for (var i = 0; i < rows.length; i++) {
    rows[i].innerHTML = "";
  }

  var activeCat = document.getElementsByClassName("active");

  for (var i = 0; i < activeCat.length; i++) {
    activeCat[i].classList.remove("active");
  }

  document.getElementById('dashboard').classList.add("active");

  var walletAddress = document.getElementsByClassName("navbar-brand");
  walletAddress[0].innerHTML = "Address: " + address;

  getTokens();
  getBalances();
  getPrices();
}

function getTokens(){
  $.post( "../php/getTokens.php", function( data ) {
    tokens = data;
  });
}

function getBalances(){
  $.post("../php/getBalance.php",
  {
    address: address
  },
  function(data, status){
    balances = data;
  });
}

function getPrices(){
  $.post( "../php/getPrices.php", function( data ) {

    prices = data;

    var obj = JSON.parse(prices);
    btc_usd = obj[0].price_usd;
    xcp_usd = obj[1].price_usd;
  });
  displayBalances();
}

function displayBalances(){

}
